# An imap biff program

Imap biff allows to watch imap mail activity on multiple accounts and start a hook in case of mail arrival.


## Usage

Copy load.template.lisp to load.lisp and adapt it to your needs.  
Add account credential in the ~/.authinfo file

load the file load.lisp into your lisp process.

You need to adjust the hook \*mail-arrival-hook\*.  
You can tweak the pooling or idle timeout depending the imap server capability.

#### Licence:

**MIT**

