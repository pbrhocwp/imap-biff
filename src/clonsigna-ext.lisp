;; MIT License
;;
;; Copyright (c) 2019 Philippe Brochard
;;
;; Permission is hereby granted, free of charge, to any person obtaining a copy
;; of this software and associated documentation files (the "Software"), to deal
;; in the Software without restriction, including without limitation the rights
;; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
;; copies of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:
;;
;; The above copyright notice and this permission notice shall be included in all
;; copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.

(in-package :clonsigna)

(defmethod wait-for-activity (sock-id (is imap-socket) &optional (timeout (* 15 60)))
  (imap-socket-send-command is :idle)
  (when (char= (char (string-trim " " (%read-line is)) 0) #\+)
	(format t "Waiting activity for ~a seconds for ~a~%" timeout sock-id)
	(handler-case
		(trivial-timeout:with-timeout (timeout)
		  (%read-line is))
	  (trivial-timeout:timeout-error (e)
		(declare (ignorable e))
		(format t "timeout of ~a seconds exausted for ~a. Abording idle command~%" timeout sock-id)))
	(imap-socket-flush-buffer is)
	(format (imap-socket-socket is) "DONE~a~a" #\Return #\Linefeed)
	(finish-output (imap-socket-socket is))
	(imap-socket-read-reply is)))

