;; MIT License
;;
;; Copyright (c) 2019 Philippe Brochard
;;
;; Permission is hereby granted, free of charge, to any person obtaining a copy
;; of this software and associated documentation files (the "Software"), to deal
;; in the Software without restriction, including without limitation the rights
;; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
;; copies of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:
;;
;; The above copyright notice and this permission notice shall be included in all
;; copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.

(in-package :imap-biff)

(defun parse-authinfo-line (line)
  (loop for (k v) on (cl-ppcre:split "\\s+" (string-trim " " line)) by #'cddr
				collect (cons (intern (string-upcase k) :keyword) v)))

(defun parse-authinfo ()
  (with-open-file (stream "~/.authinfo" :direction :input)
	(loop for line = (read-line stream nil nil)
	   while line
	   collect (parse-authinfo-line line))))

(defun find-authinfo-cred (host port login &optional authinfo)
  (let ((authinfo (or authinfo (parse-authinfo))))
	(dolist (l authinfo)
	  (when (and (string= (cdr (assoc :machine l)) host)
				 (string= (cdr (assoc :port l)) port)
				 (string= (cdr (assoc :login l)) login))
		(return (cdr (assoc :password l)))))))
