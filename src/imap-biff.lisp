;; MIT License
;;
;; Copyright (c) 2019 Philippe Brochard
;;
;; Permission is hereby granted, free of charge, to any person obtaining a copy
;; of this software and associated documentation files (the "Software"), to deal
;; in the Software without restriction, including without limitation the rights
;; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
;; copies of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:
;;
;; The above copyright notice and this permission notice shall be included in all
;; copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.

(in-package imap-biff)

(defparameter *biff-threads* nil)

(defparameter *sleep-delay* 20)
(defparameter *idle-delay* (* 15 60))

(defparameter *message-arrival-hook* nil)

(defun sock-id (sock cred)
  (format nil "~A@~A:~A" (first cred) (imap-socket-remote-host sock) (imap-socket-remote-port sock)))

(defun support-idle-p (sock)
  (equal (getf (parse-capability (cmd-capability sock)) :idle) '(T)))

(defun messages-count (sock-id sock)
  (with-report-errors ("counting message for ~A" sock-id)
	(getf (parse-status (cmd-status sock "inbox")) :messages)))

(defun biff (sock cred)
  (let ((sock-id (sock-id sock cred)))
	(loop
	   (with-report-errors ("processing ~A" sock-id)
		 (cmd-connect sock)
		 (apply #'cmd-login sock cred)
		 (cmd-select sock "inbox")
		 (let ((count (messages-count sock-id sock))
			   (support-idle-p (support-idle-p sock)))
		   (loop
			  (let ((new-count (messages-count sock-id sock)))
				(format t "Processing ~A: ~A~%" sock-id new-count)
				(unless (= count new-count)
				  (format t "   ~A -> ~A new message - ~A~%" sock-id (- new-count count) (get-format-date (get-universal-time)))
				  (when *message-arrival-hook*
					(funcall *message-arrival-hook* sock cred))
				  (setf count new-count))
				(format t "    ~A: done~%" sock-id))
			  (if support-idle-p
				  (clonsigna::wait-for-activity sock-id sock *idle-delay*)
				  (sleep *sleep-delay*)))))
	   (sleep *sleep-delay*))))

(defun biff-thread (sock cred)
  (let ((thread (bordeaux-threads:make-thread (lambda ()
												(biff sock cred)))))
	(push thread *biff-threads*)))

(defun imap-resource (host port login ssl-p &optional authinfo)
  (list (make-imap :host host :port port :ssl-p ssl-p)
		(list login (find-authinfo-cred host (write-to-string port) login authinfo))))

(defun stop-imap-biff ()
  (dolist (th *biff-threads*)
	(bordeaux-threads:destroy-thread th)))

(defun main (account-list)
  "Start an imap biff threa for each account in account-list.
Account is a list like (host port login ssl-p)
The password is retreived form the .authinfo file"
  (setf *biff-threads* nil)
  (let ((authinfo (parse-authinfo)))
	(dolist (account account-list)
	  (destructuring-bind (host port login ssl-p) account
		(apply #'biff-thread (imap-resource host port login ssl-p authinfo))))))

