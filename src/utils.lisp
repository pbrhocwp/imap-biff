;; MIT License
;;
;; Copyright (c) 2019 Philippe Brochard
;;
;; Permission is hereby granted, free of charge, to any person obtaining a copy
;; of this software and associated documentation files (the "Software"), to deal
;; in the Software without restriction, including without limitation the rights
;; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
;; copies of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:
;;
;; The above copyright notice and this permission notice shall be included in all
;; copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.

(in-package :imap-biff)

(defun get-format-date (time)
  (multiple-value-bind
		(second minute hour day month year day-of-week dst-p tz)
	  (decode-universal-time time)
	(declare (ignore day-of-week dst-p tz))
	(format nil "~2,'0d/~2,'0d/~4,'0d ~2,'0d:~2,'0d:~2,'0d" day month year hour minute second)))

(defmacro with-report-errors ((fmt &rest args) &body body)
  `(handler-case
	   (progn
		 ,@body)
	 (error (e)
	   (let ((*print-escape* nil))
		 (format t "~&** Error while ~A: ~S~%" (format nil ,fmt ,@args) e)
		 (print-object e *standard-output*)
		 (format t "~%")))))
