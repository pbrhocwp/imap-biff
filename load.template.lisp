;; MIT License
;;
;; Copyright (c) 2019 Philippe Brochard
;;
;; Permission is hereby granted, free of charge, to any person obtaining a copy
;; of this software and associated documentation files (the "Software"), to deal
;; in the Software without restriction, including without limitation the rights
;; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
;; copies of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:
;;
;; The above copyright notice and this permission notice shall be included in all
;; copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.

(require :asdf)

(load "imap-biff.asd")

(require :imap-biff)

(in-package :imap-biff)

(setf *message-arrival-hook* (lambda (sock cred)
							   (format t "~&    ==> Got a message for ~A on ~A:~A~%"
									   (first cred)
									   (imap-socket-remote-host sock)
									   (imap-socket-remote-port sock))))

(main '(("mail.account.com" 993 "your_login" t)
		("imap.mail.com" 993 "other_login" t)
		("imap.mail.com" 143 "another-login" nil)))
